# cd-aws-cloudformation-ecs
cd-aws-cloudformation-ecs demo

# PreRequisities

- AWS CLI
- AWS ECR Image with timeoff-management-application app

# How to Run this AWS CloudFormation Deployment


Create a Fork of the application [timeoff-management-application](https://github.com/arkhoss/timeoff-management-application).

Once created/forked the repository, create an AWS ECR repo, build the image and push it.

```bash
git clone https://github.com/arkhoss/timeoff-management-application.git && cd timeoff-management-application
```

> aws cli v1

```bash
$(aws ecr get-login --no-include-email --region us-east-1)
```

> aws cli v2

```bash
aws ecr get-login-password | docker login --username AWS --password-stdin ${AWS-AccountId}.dkr.ecr.${region}.amazonaws.com/timeoff-management-application

docker build -t timeoff-management-application .

docker tag timeoff-management-application:latest ${AWS-AccountId}.dkr.ecr.${region}.amazonaws.com/timeoff-management-application:latest

docker push ${AWS-AccountId}.dkr.ecr.${region}.amazonaws.com/timeoff-management-application:latest
```

Then proceed to deploy the Application with the CloudFormation Template below, click the rocket!

Deploy | Region Name | Region | Launch Types
:---: | ------------ | ------------- | -------------
[🚀][us-east-1] | US East (N. Virginia) | us-east-1 | Fargate

[us-east-1]: https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/create/review?stackName=ECS-ContinuousDeployment&templateURL=https://s3.amazonaws.com/cd-aws-cloudformation-ecs/cd-aws-cloudformation-ecs.yml&param_LaunchType=Fargate

This repository is being created for Gorilla reasons ^^
